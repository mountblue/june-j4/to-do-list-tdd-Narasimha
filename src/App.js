import React, { Component } from 'react';
import './App.css';
import Title from "./Title.js"
import ToDoContainer from './ToDoContainer';

class App extends Component {
  render() {
    return (
      <div>
        <Title/>
        <ToDoContainer/>
      </div>
    );
  }
}

export default App;
