import React from 'react';
import ReactDOM from 'react-dom';
import enzyme from "enzyme"
import { shallow, mount, render, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import App from './App';
import Title from "./Title";
import ToDoContainer from './ToDoContainer';

xit('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

xit('should contain <Title> component', () => {
  const wrapper = shallow(<App/>);x
  expect(wrapper.containsAllMatchingElements([
    <Title />,
    <ToDoContainer />
  ])).toEqual(true);
});


