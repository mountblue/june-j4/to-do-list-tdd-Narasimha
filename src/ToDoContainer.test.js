import React from 'react';
import ReactDOM from 'react-dom';
import enzyme from "enzyme"
import { shallow, mount, render, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import ToDoContainer from "./ToDoContainer";

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ToDoContainer />, div);
    ReactDOM.unmountComponentAtNode(div);
});

xit("should start with an empty list", () => {
    const wrapper = shallow(<ToDoContainer/>);
    expect(wrapper.state("listItems")).toEqual([]);
});
  
xit("add items to the to-do list", () => {
    const wrapper = shallow(<ToDoContainer/>);
    let text = "Hello World!";
    wrapper.instance().createList(text);
    expect(wrapper.state("listItems")).toEqual([text]);
})

xit("deletes items from the todolist", () => {
    const wrapper = shallow(<ToDoContainer/>);
    let text = "mango";
    wrapper.instance().deleteList(text);
    expect(wrapper.state("listItems")).toEqual(["apple","grapes","banana"]);
})


it('should accept input', () => {
    const wrapper = shallow(<ToDoContainer  />);
    const input = wrapper.find('input');
    input.simulate('change', {target: { value: 'Resin' }});
    expect(wrapper.state('text')).to.equal('Resin');
    expect(input.prop('value')).to.equal('Resin');
});
