import React, { Component } from 'react';

// import Title from "./Title.js"

class Title extends Component {
  render() {
    return (
      <div className="title-div">
        <h1 className="title-text">To Do List</h1>
      </div>
    );
  }
}

export default Title;

