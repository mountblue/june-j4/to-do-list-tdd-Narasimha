import React, { Component } from 'react';

class ToDoContainer extends Component {
    constructor(props){
        super(props);

        this.state = {
            listItems: ["apple","grapes","banana","mango"]
        }
    }

    createList(text){
        this.setState({
            listItems: [].concat(this.state.listItems).concat([text])
        })
    }

    deleteList(text){
        var temp = this.state.listItems.filter(item => item != text);

        this.setState({
            listItems: temp
        })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.createList}>
                    <input type="text"/>
                </form>
            </div>
        );
    }
}

export default ToDoContainer;
